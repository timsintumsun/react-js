import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// Pages
import Login from './pages/Login';
import Register from './pages/Register';


function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/Register">
            <Register />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}


export default App;


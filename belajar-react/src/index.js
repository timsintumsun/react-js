import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from "redux";
import { Provider } from 'react-redux';

//initial Global State
const globalState = {
  count: 0,
};

const HANDLE_PLUS = 'HANDLE_PLUS';
const HANDLE_MINUS = 'HANDLE_MINUS';

//create Reducer
const rootReducer = (state = globalState, action) => {
  if (action.type === HANDLE_PLUS) {
    return {
      ...state,
      count: state.count + 1,
    };
  } else if (action.type === HANDLE_MINUS) {
    return {
      ...state,
      count: state.count - 1,
    };
  }
  return state;
};

// create store
const rootStore = createStore(rootReducer);

ReactDOM.render(

  <React.StrictMode>
    <Provider store={rootStore}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

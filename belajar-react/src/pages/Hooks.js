import React, { useState } from 'react';

// Cara 1:
// const Hooks = () => {
//     const [nama, setName] = useState('Gilang');
//     const [umur, setUmur] = useState('21');
//     const [lokasi, setLocation] = useState('Tangerang');
//     console.log({ nama });
//     console.log({ umur });

//     const ubahIdentity = () => {
//         setName('Nana');
//         setUmur('20');
//         setLocation('Bandung');
//     };

//     return (
//         <div>
//             <p>Hooks Pertama</p>
//             <p>Nama saya adalah {nama}</p>
//             <p>Umur saya adalah {umur}</p>
//             <p>Umur saya adalah {lokasi}</p>
//             <button onClick={ubahIdentity}>Ubah Identity</button>
//         </div>
//     );
// }

// Cara 2:
// const Hooks = () => {
//     const [nama, setName] = useState('Jennie');
//     const [isName, setIsName] = useState(true);
//     const [colorInput, setColorInput] = useState('red');

//     const ubahIdentity = () => {
//         if (isName) {
//             setName('Jisoo');
//             setIsName(false);
//             setColorInput('green');
//         } else {
//             setName('Jennie');
//             setIsName(true);
//             setColorInput('red');
//         }
//     };

//     return (
//         <div>
//             <p>Hooks Pertama</p>
//             <p style={{ color: colorInput }}>Nama saya adalah {nama}</p>
//             <button onClick={ubahIdentity}>Ubah Identity</button>
//         </div>
//     );
// }

// Cara 3:
const Hooks = () => {
    const [isName, setIsName] = useState(true);
    const [colorInput, setColorInput] = useState(true);

    const ubahIdentity = () => {
        setIsName(!isName);
        setColorInput(!colorInput);

    };
    console.log(isName);

    return (
        <div>
            <p>Hooks Pertama</p>
            {/* Ternary Operator from Javascript */}
            <p style={{ color: colorInput ? 'red' : 'green' }}>Nama saya adalah {isName ? 'Gilang' : 'Nana'}</p>
            <button onClick={ubahIdentity}>Ubah Identity</button>
        </div>
    );
}

export default Hooks;

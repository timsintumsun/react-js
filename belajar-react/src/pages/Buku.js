import React, { useState } from 'react';
import imgBooks from '../assets/images/buku.jpg';

const Buku = () => {
    const [books, setBooks] = useState([]);
    // const [books, setBooks] = useState([]);

    const addBooks = () => {
        // setBooks({ 'id': });
        const bookTemp = [...books];
        const newBooks = { id: books.length + 1 }
        const result = bookTemp.concat(newBooks);
        console.log(result);
        setBooks(result);
    }

    const removeBooks = () => {
        const bookTemp = [...books];
        const result = bookTemp.slice(0, - 1);
        console.log(result);
        setBooks(result);

    }
    return (
        <div>
            <h1>BUKU</h1>
            <button onClick={() => addBooks()}>Add Book </button>
            <button onClick={() => removeBooks()}>Remove Book </button>

            <h2>List Buku:</h2>
            {books.map((list, index) => (
                // Cara 1:
                <ul key={index}>
                    <li><img src={imgBooks}></img> Buku {list.id}</li>
                </ul>
            ))}
        </div>
    );
}

export default Buku;
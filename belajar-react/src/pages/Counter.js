import React, { useState } from 'react';
import Test from '../components/Test';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

const HANDLE_PLUS = 'HANDLE_PLUS';
const HANDLE_MINUS = 'HANDLE_MINUS';

// cara lain handle props
// const Counter = ({ count, counterMinusFunction, counterPlusFunction }) => {
const Counter = (props) => {
    // cara lain handle props
    // const { count, counterMinusFunction, counterPlusFunction } = props;

    const [nama, setNama] = useState('Gilang');
    return (
        <div>
            <Test nama={nama} />
            <Button color='primary' variant='contained' onClick={() => setNama('Nana')}>Ubah Nama </Button>


            <div>-------</div>
            <p>kamu ngeklik sebanyak {props.count} kali</p>
            <Button color='secondary' variant='contained' onClick={props.counterMinusFunction}>minus</Button>
            <Button color='primary' variant='contained' onClick={props.counterPlusFunction}>plus</Button>
        </div>
    );
}

const mapStateToProps = (globalState) => {
    return {
        count: globalState.count,
    };
}

const mapDispatchToProps = (dispatch) => {

    return {
        counterPlusFunction: () => dispatch({ type: HANDLE_PLUS }),
        counterMinusFunction: () => dispatch({ type: HANDLE_MINUS }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
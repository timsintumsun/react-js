import React, { useEffect, useState } from 'react';
import axios from 'axios';
/*
    1. componentDidMount
    2. componentDidUpdate
    3. componentWillUnmount
    
    FAKE API:
    https://reqres.in/api/users?page=2
    https://jsonplaceholder.typicode.com/photos
*/

const Contributor = () => {
    const [users1, setUsers1] = useState([]);
    const [users2, setUsers2] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            // const result1 = await axios.get('https://jsonplaceholder.typicode.com/photos');
            // setUsers1(result1.data);

            const result2 = await axios.get('https://reqres.in/api/users?page=2');
            setUsers2(result2.data.data);
        }
        fetchData();

    }, []);

    console.log('USERS 2 :: ', users2);
    // console.log('USERS :: ', users1);

    return (
        <div>
            <h2>Contributor Case 1:</h2>
            {users2.map((list, index) => (
                // Cara 1:
                <ul key={index}>
                    <li><img src={list.avatar} alt="" /></li>
                    <li>{list.first_name}</li>
                </ul>
            ))}
            {/* <h2>Contributor Case 2:</h2>
            {users1.map((list, index) => (
                // Cara 1:
                <ul key={index}>
                    <li><img src={list.thumbnailUrl} alt="" /></li>
                    <li>{list.title}</li>
                </ul>
            ))} */}
        </div>
    );

    // return (
    //     <div>
    //         <h1>Contributor:</h1>
    //         {users.map((list, index) => (
    //             // Cara 2
    //             <div key={index} >
    //                 <img src={list.avatar} alt="" />
    //                 <p>{`${list.first_name} ${list.last_name}`}</p>
    //                 <p>{list.email}</p>
    //             </div>
    //         ))}
    //     </div>
    // );

}

export default Contributor;
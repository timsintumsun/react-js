import React from 'react';
import { connect } from 'react-redux';

const Home = (props) => {
    return (
        <div>
            <p>Home</p>
            <p>REDUX GLOBAL NIH : {props.count}</p>
        </div>
    );
}
const mapStateToProps = (globalState) => {
    return {
        count: globalState.count,
    }
}
export default connect(mapStateToProps)(Home);
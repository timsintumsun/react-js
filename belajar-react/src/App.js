import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import AboutMe from './pages/AboutMe';
import Counter from './pages/Counter';
import Hooks from './pages/Hooks';
import React from 'react';
import Navbar from './components/Navbar';
import Contributor from './pages/Contributor';
import Buku from './pages/Buku';
import './assets/css/navbar.css';


function App() {

  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about-me">
          <AboutMe />
        </Route>
        <Route path="/counter">
          <Counter />
        </Route>
        <Route path="/hooks">
          <Hooks />
        </Route>
        <Route path="/contributor">
          <Contributor />
        </Route>
        <Route path="/buku">
          <Buku />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

// function App() {
//   const [nama, setNama] = useState('Gilang');
//   return (
//     <div className="App">
//       <AboutMe />
//       <Test nama={nama} />
//       <button onClick={() => setNama('Nana')}>Ubah Nama </button>
//     </div>

//   );
// }

// class App extends Component {
//   render() {
//     return (
//       < div className="App" >
//         <Test nama={nama} />
//         <button onClick={() => setNama('Nana')}>Ubah Nama </button>
//       </div >
//     );
//   }
// }

export default App;
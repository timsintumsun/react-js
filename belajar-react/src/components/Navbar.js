import React from 'react';
import { Link } from 'react-router-dom';
// import '../assets/css/navbar.css';

const Navbar = () => {
    return (
        <div className='navbar'>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/about-me">AboutMe</Link></li>
                <li><Link to="/counter">Counter</Link></li>
                <li><Link to="/hooks">Hooks Use State</Link></li>
                <li><Link to="/contributor">Contributor</Link></li>
                <li><Link to="/buku">Buku</Link></li>
            </ul>
        </div>
    );
}

export default Navbar;
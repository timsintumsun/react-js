// vanilla redux
const redux = require('redux');
const defaultState = {
    nilai: 25,
    umur: 50
};
console.log('defaultState :: ', defaultState);
// const defaultStateNum = 1;

const TAMBAH_UMUR = 'TAMBAH_UMUR';
const TAMBAH_NILAI = 'TAMBAH_NILAI';

// reducer
// cara if-else
// const rootReducer = (state = defaultState, action) => {
//     if (action.type === TAMBAH_UMUR) {
//         return {
//             ...state,
//             umur: state.umur + 60,
//         }
//     }
//     if (action.type === TAMBAH_NILAI) {
//         return {
//             ...state,
//             nilai: state.nilai + action.payload,
//             status: action.data,
//         }
//     }
//     return state;
// };

// cara switch-case
const rootReducer = (state = defaultState, action) => {
    switch (action.type) {
        case TAMBAH_UMUR:
            return {
                ...state,
                umur: state.umur + 60,
            };
        case TAMBAH_NILAI:
            return {
                ...state,
                nilai: state.nilai + action.payload,
                status: action.data,
            };
        default:
            return state;
    }
};

// store
const store = redux.createStore(rootReducer);

// action
store.dispatch({ type: TAMBAH_UMUR });
console.log('TAMBAH_UMUR :: ', store.getState());
store.dispatch({ type: TAMBAH_NILAI, payload: 75, data: 'Nikah' });
console.log('TAMBAH_NILAI :: ', store.getState());